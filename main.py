import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:33:01.684683
#2018-02-27 09:47:01.737552
#2018-02-27 10:02:02.091421
#2018-02-27 10:28:01.985129
#2018-02-27 10:55:04.034801
#2018-02-27 11:19:01.926420
#2018-02-27 11:43:02.115787
#2018-02-27 12:02:01.359797
#2018-02-27 12:29:01.677858
#2018-02-27 12:48:01.238729
#2018-02-27 13:19:01.603880
#2018-02-27 13:39:01.188341
#2018-02-27 13:54:02.162431
#2018-02-27 14:23:02.001297
#2018-02-27 14:47:01.640852
#2018-02-27 15:08:01.743878
#2018-02-27 15:21:01.843380
#2018-02-27 15:47:01.692357
#2018-02-27 16:24:01.877755
#2018-02-27 17:31:01.827252
#2018-02-27 18:16:01.534678
#2018-02-27 19:18:01.682089
#2018-02-27 20:33:01.555168